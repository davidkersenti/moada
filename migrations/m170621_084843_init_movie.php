<?php

use yii\db\Migration;

class m170621_084843_init_movie extends Migration
{
   public function up()
    {
        $this->createTable('movie', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'genre' => $this->string(45),
            'min_age' => $this->integer(),
            'grade' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('movie');
    }
}
