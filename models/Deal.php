<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadId
 * @property string $name
 * @property integer $amount
 *
 * @property Deal $lead
 * @property Deal[] $deals
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'leadId', 'name', 'amount'], 'required'],
            [['id', 'leadId', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 20],
            [['leadId'], 'exist', 'skipOnError' => true, 'targetClass' => Deal::className(), 'targetAttribute' => ['leadId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLead()
    {
        return $this->hasOne(Deal::className(), ['id' => 'leadId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeals()
    {
        return $this->hasMany(Deal::className(), ['leadId' => 'id']);
    }
}
